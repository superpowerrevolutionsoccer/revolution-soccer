﻿using UnityEngine;
using System.Collections;

public class Goal : MonoBehaviour {

    //Properties

    [SerializeField]
    private bool isPlayerGoalpost;

    //Unity functions

    void OnTriggerEnter(Collider coll)
    {
        if(coll.gameObject.CompareTag("Ball"))
        {
            if(isPlayerGoalpost)
            {
                Score.Instance.IAScore += 1;
                Debug.Log("Partido: " + Score.Instance.PlayerScore + "-" + Score.Instance.IAScore);
                //TODO: Volver a las posciones iniciales del script CreadorJugadores
            }
            else
            {
                Score.Instance.PlayerScore += 1;
                Debug.Log("Partido: " + Score.Instance.PlayerScore + "-" + Score.Instance.IAScore);
                //TODO: Volver a las posciones iniciales del script CreadorJugadores
            }
        }
    }
}