﻿using UnityEngine;
using System.Collections;

public class Score : MonoBehaviour {

    //Singleton

    private static Score instance;
    public static Score Instance
    {
        get
        {
            return instance;
        }
    }

    //Properties

    [HideInInspector]
    public int PlayerScore;
    [HideInInspector]
    public int IAScore;
    
    //Unity functions

    void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    void Start()
    {
        PlayerScore = 0;
        IAScore = 0;
    }
}