﻿using UnityEngine;
using System.Collections;

public class Ontrigger : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.name == "DefensaEquoBlanco") {
            Control1.controltrigger1 = 1;
        }
        else if (other.gameObject.name == "MedioCampoEquipoBlanco") {
            Control1.controltrigger1 = 2;
        }
        else if (other.gameObject.name == "DelanteroEquoBlanco")
        {
            Control1.controltrigger1 = 3;
        } 
        else if (other.gameObject.name == "DefensaEquoYellow")
        {
            Control2.controltrigger2 = 1;
        }
        else if (other.gameObject.name == "MedioCampoEquipoYellow")
        {
            Control2.controltrigger2 = 2;
        }
        else if (other.gameObject.name == "DelanteroEquoYellow")
        {
            Control2.controltrigger2 = 3;
        }
    }
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "MedioCampoEquipoBlanco") {
            Control1.controltrigger1 = 0;
        } else if (other.gameObject.name == "DelanteroEquoBlanco") {
            Control1.controltrigger3 = 0;
        }

    }
    }
