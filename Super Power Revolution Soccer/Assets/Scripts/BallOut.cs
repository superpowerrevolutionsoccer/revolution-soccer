﻿using UnityEngine;
using System.Collections;

public class BallOut : MonoBehaviour {

    //Properties

    private bool isThrowIn;
    private bool resetBallPosition;

    [SerializeField]
    private float timeToThrow;
    private float timeToThrowTemp;

    private Vector3 ballEnterPosition;

    //Unity functions

    void Start()
    {
        timeToThrowTemp = timeToThrow;
    }

    void Update()
    {
        if(isThrowIn) //El jugador esta en saque de banda
        {
            timeToThrow -= Time.deltaTime;

            if (timeToThrow <= 0)
            {
                timeToThrow = timeToThrowTemp;
                resetBallPosition = true;
                isThrowIn = false; //El jugador ya no esta en saque de banda
            }
        }
    }

    //Trigger functions

    void OnTriggerEnter(Collider coll)
    {
        if(coll.gameObject.CompareTag("Ball"))
        {
            coll.gameObject.transform.parent = null;
            ballEnterPosition = coll.gameObject.GetComponent<Transform>().position;
            isThrowIn = true;
        }
    }

    void OnTriggerStay(Collider coll)
    {
        if (coll.gameObject.CompareTag("Ball"))
        {
            if (resetBallPosition)
            {
                coll.gameObject.transform.parent = null;
                coll.gameObject.transform.position = ballEnterPosition;
                resetBallPosition = false;
            }
        }
    }
}