﻿using UnityEngine;
using System.Collections;

public class ShootBall : MonoBehaviour {

    //Properties

    private float speed;
    [SerializeField]
    private float speedGain;
    [SerializeField]
    private float speedLimit;

    //Unity functions

	void Update ()
    {
		if (Input.GetButtonDown ("Fire1"))
        {
			speed = 0;
        }

        if (Input.GetButton("Fire1"))
        {
			speed+=speedGain*Time.deltaTime;

            if (speed >= speedLimit)
				Shoot ();
		}

        if (Input.GetButtonUp ("Fire1"))
        {
			Shoot ();
		}
	}

    //Class functions

	void Shoot()
    {
        gameObject.transform.parent = null;
        GetComponent<Rigidbody>().isKinematic = false;
		GetComponent<Rigidbody> ().velocity=transform.right * speed;
	}
}