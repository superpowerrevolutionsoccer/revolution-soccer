﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonsFunctions : MonoBehaviour {

    //Properties

    public GameObject[] buttons;
    [SerializeField]
    private float soundGainValue;
    [SerializeField]
    private Renderer colombia;
    [SerializeField]
    private Renderer japan;

    [Header("References")]

    [SerializeField]
    private AudioSource audioSource;

    private int currentMenu;
    private int volume;
    private int teamsSelected;

    //Unity functions

    void Start ()
    {
        buttons[0].SetActive(true);

        for(int i = 1; i<buttons.Length; i++)
        {
            buttons[i].SetActive(false);
        }

        teamsSelected = 0;
        currentMenu = 0;
	}

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape) && currentMenu >= 1)
        {
            buttons[currentMenu].SetActive(false);
            buttons[0].SetActive(true);
        }

        if (teamsSelected >= 2)
        {
            SceneManager.LoadScene("Game"); //Load the game scene
        }
    }

    //Buttons functions

    //Main
    public void Play()
    {
        buttons[0].SetActive(false);
        buttons[2].SetActive(true);

        currentMenu = 2;
    }

    public void Options()
    {
        buttons[0].SetActive(false);
        buttons[1].SetActive(true);

        currentMenu = 1;
    }

    public void Exit()
    {
        Application.Quit();
        Debug.Log("Exit");
    }

    //Options
    public void Formations()
    {
        //NONE
    }

    public void Velocity()
    {
        //NONE
    }

    public void Stadium()
    {
        //NONE
    }

    public void SoundPlus()
    {
        if(audioSource.volume <= 1-soundGainValue)
        {
            audioSource.volume += soundGainValue;
        }
        else
        {
            Debug.Log("The sound is at his maximum");
        }
    }

    public void SoundLess()
    {
        if (audioSource.volume >= 1 - (1-soundGainValue))
        {
            audioSource.volume -= soundGainValue;
        }
        else
        {
            Debug.Log("The sound is at his minimum");
        }
    }

    //Game Modes
    public void Match()
    {
        buttons[2].SetActive(false);
        buttons[3].SetActive(true);

        currentMenu = 3;
    }

    public void Tournament()
    {
        //NONE
    }

    public void Friendly()
    {
        //NONE
    }

    //Teams
    public void Colombia()
    {
        Debug.Log("Colombia");
        teamsSelected++;

        if (teamsSelected == 1)
        {
            StaticColors.playerTeamColor = "yellow";
        }
        else
        {
            StaticColors.iaTeamColor = "yellow";
        }
    }

    public void Japan()
    {
        Debug.Log("Japan");
        teamsSelected++;

        if (teamsSelected == 1)
        {
            StaticColors.playerTeamColor = "blue";
        }
        else
        {
            StaticColors.iaTeamColor = "blue";
        }
    }
}