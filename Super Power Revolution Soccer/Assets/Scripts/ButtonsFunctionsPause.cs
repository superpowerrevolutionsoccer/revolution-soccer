﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonsFunctionsPause : MonoBehaviour {

    //Properties

	void Update ()
    {
        if(Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
        {
            PauseManager.Instance.isPaused = !PauseManager.Instance.isPaused;
        }
	}

    //Buttons functions

    //Pause
    public void Resume()
    {
        PauseManager.Instance.isPaused = false;
    }

    public void Exit()
    {
        SceneManager.LoadScene("Menu"); //Load the menu scene
    }
}