﻿using UnityEngine;
using System.Collections;

public class CreadorJugadores : MonoBehaviour {
    [SerializeField]
    private GameObject JugadorIA;

    [SerializeField]
    private GameObject JugadorController;



    //Spawns equipo Azul
    [SerializeField]
    private GameObject SDefensaAzul1;
    [SerializeField]
    private GameObject SDefensaAzul2;
    [SerializeField]
    private GameObject SDefensaAzul3;
    [SerializeField]
    private GameObject SDefensaAzul4;
    [SerializeField]
    private GameObject SMedioCampoAzul1;
    [SerializeField]
    private GameObject SMedioCampoAzul2;
    [SerializeField]
    private GameObject SMedioCampoAzul3;
    [SerializeField]
    private GameObject SDelanteroAzul1;
    [SerializeField]
    private GameObject SDelanteroAzul2;
    [SerializeField]
    private GameObject SDelanteroAzul3;

    //Spawns equipo Amarillo
    [SerializeField]
    private GameObject SDefensaAmarillo1;
    [SerializeField]
    private GameObject SDefensaAmarillo2;
    [SerializeField]
    private GameObject SDefensaAmarillo3;
    [SerializeField]
    private GameObject SDefensaAmarillo4;
    [SerializeField]
    private GameObject SMedioCampoAmarillo1;
    [SerializeField]
    private GameObject SMedioCampoAmarillo2;
    [SerializeField]
    private GameObject SMedioCampoAmarillo3;
    [SerializeField]
    private GameObject SDelanteroAmarillo1;
    [SerializeField]
    private GameObject SDelanteroAmarillo2;
    [SerializeField]
    private GameObject SDelanteroAmarillo3;






    // Use this for initialization
    void Start () {
        for (int i = 0; i < 20; i++) {

            

            if (i == 0) {
                GameObject Jugador = Instantiate(JugadorIA, SDefensaAzul1.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
                Jugador.tag = "DefensaBlanco";
                Jugador.name = "DB1";


            }
            else if(i == 1){
                GameObject Jugador = Instantiate(JugadorIA, SDefensaAzul2.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
                Jugador.tag = "DefensaBlanco";
                Jugador.name = "DB2";
            }
            else if (i == 2)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDefensaAzul3.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
                Jugador.tag = "DefensaBlanco";
                Jugador.name = "DB3";
            }
            else if (i == 3)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDefensaAzul4.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
                Jugador.tag = "DefensaBlanco";
                Jugador.name = "DB4";
            }
            else if (i == 4)
            {
                GameObject Jugador = Instantiate(JugadorIA, SMedioCampoAzul1.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
                Jugador.tag = "MedioBlanco";
                Jugador.name = "MB1";
            }
            else if (i == 5)
            {
                GameObject Jugador = Instantiate(JugadorIA, SMedioCampoAzul2.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
                Jugador.tag = "MedioBlanco";
                Jugador.name = "MB2";
            }
            else if (i == 6)
            {
                GameObject Jugador = Instantiate(JugadorIA, SMedioCampoAzul3.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
                Jugador.tag = "MedioBlanco";
                Jugador.name = "MB3";
            }
            else if (i ==7)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDelanteroAzul1.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
                Jugador.tag = "DelanteroBlanco";
                Jugador.name = "DEB1";
            }
            else if (i == 8)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDelanteroAzul2.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
                Jugador.tag = "DelanteroBlanco";
                Jugador.name = "DEB2";
            }
            else if (i == 9)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDelanteroAzul3.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.white;
                Jugador.tag = "DelanteroBlanco";
                Jugador.name = "DEB3";
            }
            else if (i == 10)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDefensaAmarillo1.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.yellow;
                Jugador.tag = "DefensaY";
                Jugador.name = "DY1";

            }
            else if (i == 11)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDefensaAmarillo2.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.yellow;
                Jugador.tag = "DefensaY";
                Jugador.name = "DY2";
            }
            else if (i == 12)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDefensaAmarillo3.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.yellow;
                Jugador.tag = "DefensaY";
                Jugador.name = "DY3";
            }
            else if (i == 13)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDefensaAmarillo4.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.yellow;
                Jugador.tag = "DefensaY";
                Jugador.name = "DY4";
            }
            else if (i == 14)
            {
                GameObject Jugador = Instantiate(JugadorIA, SMedioCampoAmarillo1.transform.position, Quaternion.identity) as GameObject;
               
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color= Color.yellow;
                Jugador.tag = "MedioY";
                Jugador.name = "MY1";
            }
            else if (i == 15)
            {
                GameObject Jugador = Instantiate(JugadorIA, SMedioCampoAmarillo2.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.yellow;
                Jugador.tag = "MedioY";
                Jugador.name = "MY2";
            }
            else if (i == 16)
            {
                GameObject Jugador = Instantiate(JugadorIA, SMedioCampoAmarillo3.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.yellow;
                Jugador.tag = "MedioY";
                Jugador.name = "MY3";
            }
            else if (i == 17)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDelanteroAmarillo1.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.yellow;
                Jugador.tag = "DelanteY";
                Jugador.name = "DDY1";

            }
            else if (i == 18)
            {
                GameObject Jugador = Instantiate(JugadorController, SDelanteroAmarillo2.transform.position, Quaternion.identity) as GameObject;
                Jugador.GetComponent<SkinnedMeshRenderer>().material.color = Color.yellow;
                Jugador.name = "jugador";
                Jugador.tag = "jugador";
            }
            else if (i == 19)
            {
                GameObject Jugador = Instantiate(JugadorIA, SDelanteroAmarillo3.transform.position, Quaternion.identity) as GameObject;
                Jugador.transform.FindChild("EthanBody").GetComponent<SkinnedMeshRenderer>().material.color = Color.yellow;
                Jugador.tag = "DelanteY";
                Jugador.name = "DDY3";
            }
            

        }


	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
