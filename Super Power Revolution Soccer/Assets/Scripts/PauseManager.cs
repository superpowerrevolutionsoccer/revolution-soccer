﻿using UnityEngine;
using System.Collections;

public class PauseManager : MonoBehaviour {

    //Singleton

    private static PauseManager instance;
    public static PauseManager Instance
    {
        get
        {
            return instance;
        }
    }
    
    //Properties

    [HideInInspector]
    public bool isPaused;
    [SerializeField]
    private GameObject menu;

    //Unity functions

    void Awake()
    {
        if(instance != null)
        {
            Destroy(gameObject);
        }
        else
        {
            instance = this;
        }
    }

    void Start ()
    {
        if(menu != null)
        {
            isPaused = false;
        }
        else
        {
            Debug.LogWarning("No menu attached to the script");
        }
	}

	void Update ()
    {
        if(isPaused)
        {
            Time.timeScale = 0;
            menu.SetActive(true);
        }
        else
        {
            Time.timeScale = 1;
            menu.SetActive(false);
        }
	}
}