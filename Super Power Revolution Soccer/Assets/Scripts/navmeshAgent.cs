﻿using UnityEngine;
using System.Collections;

namespace UnityStandardAssets.Characters.ThirdPerson
{
    [RequireComponent(typeof(NavMeshAgent))]
    [RequireComponent(typeof(ThirdPersonCharacter))]
    public class navmeshAgent : MonoBehaviour {

     
       

        public NavMeshAgent agent { get; private set; } // the navmesh agent required for the path finding
        public ThirdPersonCharacter character { get; private set; } // the character we are controlling
        public GameObject target; // target to aim for

        // Use this for initialization
        void Start() {
            
            agent = GetComponentInChildren<NavMeshAgent>();
            character = GetComponent<ThirdPersonCharacter>();

            agent.updateRotation = false;
            agent.updatePosition = true;
        }
       

    // Update is called once per frame
        void Update() {
            //  if (target != null)
            //  {

            if (gameObject.tag == "DefensaBlanco") {
                if (Control1.controltrigger1 == 1)
                {
                    target = GameObject.Find("Sphere");
                    agent.SetDestination(target.GetComponent<Transform>().position);

                    // use the values to move the character
                    character.Move(agent.desiredVelocity, false, false);
                }
                else {
                    if (gameObject.name == "DB1") {
                        target = GameObject.Find("DefensaEquipoA1");
                        agent.SetDestination(target.GetComponent<Transform>().position);

                        // use the values to move the character
                        character.Move(agent.desiredVelocity, false, false);
                    } else if (gameObject.name == "DB2") {
                        target = GameObject.Find("DefensaEquipoA2");
                        agent.SetDestination(target.GetComponent<Transform>().position);

                        // use the values to move the character
                        character.Move(agent.desiredVelocity, false, false);
                    }
                    else if (gameObject.name == "DB3")
                    {
                        target = GameObject.Find("DefensaEquipoA3");
                        agent.SetDestination(target.GetComponent<Transform>().position);

                        // use the values to move the character
                        character.Move(agent.desiredVelocity, false, false);
                    }
                    else if (gameObject.name == "DB4")
                    {
                        target = GameObject.Find("DefensaEquipoA4");
                        agent.SetDestination(target.GetComponent<Transform>().position);

                        // use the values to move the character
                        character.Move(agent.desiredVelocity, false, false);
                    }

                }


            } else if (gameObject.tag == "MedioBlanco") {
                if (Control1.controltrigger1 == 2)
                {
                    target = GameObject.Find("Sphere");
                    agent.SetDestination(target.GetComponent<Transform>().position);

                    // use the values to move the character
                    character.Move(agent.desiredVelocity, false, false);
                }
                else {
                    if (gameObject.name == "MB1")
                    {
                        target = GameObject.Find("MedioCampoEquipoA1");
                        agent.SetDestination(target.GetComponent<Transform>().position);

                        // use the values to move the character
                        character.Move(agent.desiredVelocity, false, false);
                    }
                    else if (gameObject.name == "MB2")
                    {
                        target = GameObject.Find("MedioCampoEquipoA2");
                        agent.SetDestination(target.GetComponent<Transform>().position);

                        // use the values to move the character
                        character.Move(agent.desiredVelocity, false, false);
                    }
                    else if (gameObject.name == "MB3")
                    {
                        target = GameObject.Find("MedioCampoEquipoA3");
                        agent.SetDestination(target.GetComponent<Transform>().position);

                        // use the values to move the character
                        character.Move(agent.desiredVelocity, false, false);
                    }


                }
            } else if (gameObject.tag == "DelanteroBlanco") {
                if (Control1.controltrigger1 == 3)
                {
                    target = GameObject.Find("Sphere");
                    agent.SetDestination(target.GetComponent<Transform>().position);

                    // use the values to move the character
                    character.Move(agent.desiredVelocity, false, false);
                }
                else {
                    if (gameObject.name == "DEB1")
                    {
                        target = GameObject.Find("DelanteroEquipoA1");
                        agent.SetDestination(target.GetComponent<Transform>().position);

                        // use the values to move the character
                        character.Move(agent.desiredVelocity, false, false);
                    }
                    else if (gameObject.name == "DEB2")
                    {
                        target = GameObject.Find("DelanteroEquipoA2");
                        agent.SetDestination(target.GetComponent<Transform>().position);

                        // use the values to move the character
                        character.Move(agent.desiredVelocity, false, false);
                    }
                    else if (gameObject.name == "DEB3")
                    {
                        target = GameObject.Find("DelanteroEquipoA3");
                        agent.SetDestination(target.GetComponent<Transform>().position);

                        // use the values to move the character
                        character.Move(agent.desiredVelocity, false, false);
                    }


                }

            }

      }

               // target = GameObject.Find("Sphere");
               // agent.SetDestination(target.GetComponent<Transform>().position);
                


                // use the values to move the character
               // character.Move(agent.desiredVelocity, false, false);
          //  }

          //  else
          //  {
                // We still need to call the character's move function, but we send zeroed input as the move param.
              //  character.Move(Vector3.zero, false, false);
           // }
        }
        //public void SetTarget(GameObject target)
       // {
        //    this.target = target;
       // }
    
}

